using Loggers.Components;

namespace Loggers
{
    public interface ILogger
    {
        void Log(LogEntry logEntry);
    }

    
}