namespace Loggers.Components
{
    public static class LogCategory
    {
        public const string Error = "Error";
        public const string Warning = "Warning";
        public const string Info = "Information";
    }
}